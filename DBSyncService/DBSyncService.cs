﻿using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DBSyncService
{
    public partial class DBSyncService : ServiceBase
    {
        private Logger _logger = LogManager.GetLogger("DBSyncService");
        private Thread _thread;

        public DBSyncService()
        {
            InitializeComponent();
            //ProcessThread(null); // For locally debug. Comment while release.
        }

        protected override void OnStart(string[] args)
        {
            try
            {
                _logger.Debug("On Start");
                _thread = new Thread(ProcessThread);
                _thread.Name = "Main";
                _thread.IsBackground = true;
                _thread.Start();

            }
            catch (Exception ex)
            {
                _logger.Error("Exception in DBSyncService.OnStart() : " + ex);
            }
        }

        private void ProcessThread(object obj)
        {
            try
            {
                _logger.Debug("***************************** Started - DB Sync Service *******************************");
                _logger.Debug("START: DB Sync Service...");
                
                DBSyncProcess oDBSyncProcess = new DBSyncProcess();
                oDBSyncProcess.StartDBSyncProcess();

                _logger.Debug("END: DB Sync Service...");
                _logger.Debug("***************************** Stopped - DB Sync Service *******************************");
            }
            catch (Exception ex)
            {
                _logger.Error("Exception in DBSyncService.ProcessThread() : " + ex);
            }
        }

        protected override void OnStop()
        {
            _logger.Debug("On Stop");
        }
    }
}

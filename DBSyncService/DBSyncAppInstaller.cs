﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.Threading.Tasks;

namespace DBSyncService
{
    [RunInstaller(true)]
    public partial class DBSyncAppInstaller : System.Configuration.Install.Installer
    {
        public DBSyncAppInstaller()
        {
            InitializeComponent();
        }
    }
}

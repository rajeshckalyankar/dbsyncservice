﻿using DBSyncService.DataAccess;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DBSyncService
{
    public class DBSyncProcess
    {
        private Thread _thread_sync_orders;
        private Thread _thread_sync_products;
        private Thread _thread_sync_order_status;
        private Logger _logger = LogManager.GetLogger("DBSyncService");
        private IntDBDataAccess oIntDBDataAccess = new IntDBDataAccess();
        private bool IsSyncingByNop = false;

        #region Application configurations


        private bool ThreadSleep
        {
            get
            {
                return Convert.ToBoolean(ConfigurationManager.AppSettings["ThreadSleep"]);
            }
        }

        private int OrderThreadSleep
        {
            get
            {
                return Convert.ToInt32(ConfigurationManager.AppSettings["OrderThreadSleep"]);
            }
        }

        private int ProductThreadSleep
        {
            get
            {
                return Convert.ToInt32(ConfigurationManager.AppSettings["ProductThreadSleep"]);
            }
        }

        private int OrderStatusThreadSleep
        {
            get
            {
                return Convert.ToInt32(ConfigurationManager.AppSettings["OrderStatusThreadSleep"]);
            }
        }

        #endregion

        public DBSyncProcess()
        {
            try
            {
                _thread_sync_orders = new Thread(SyncOrders);
                _thread_sync_orders.Name = "SyncOrders";
                _thread_sync_orders.IsBackground = true;

                _thread_sync_products = new Thread(SyncProducts);
                _thread_sync_products.Name = "SyncProducts";
                _thread_sync_products.IsBackground = true;

                _thread_sync_order_status = new Thread(SyncOrderStatus);
                _thread_sync_order_status.Name = "SyncOrderStatus";
                _thread_sync_order_status.IsBackground = true;
            }
            catch (Exception ex)
            {
                _logger.Error("Exception in DBSyncProcess.DBSyncProcess() : " + ex);
                //_logger.Error(ex.StackTrace);
            }
        }

        public void StartDBSyncProcess()
        {
            try
            {
                _thread_sync_orders.Start();
                _thread_sync_products.Start();
                _thread_sync_order_status.Start();

                _thread_sync_orders.Join();
                _thread_sync_products.Join();
                _thread_sync_order_status.Join();
            }
            catch (Exception ex)
            {
                _logger.Error("Exception in DBSyncProcess.StartDBSyncProcess() : " + ex);
            }
        }

        private void SyncOrders(object obj)
        {
            SyncOrdersProcess oSyncOrdersProcess = new SyncOrdersProcess();

            while (true)
            {
                try
                {
                    _logger.Debug("...");
                    _logger.Debug("Starting orders sync iteration...");
                    
                    _logger.Debug("Start - Updating 'IsSyncingByNop' flag = 1");

                    if (oIntDBDataAccess.UpdateSyncingByNopFlag("Order", 1))
                    {
                        IsSyncingByNop = true;

                        oSyncOrdersProcess.StartOrdersSync();

                        _logger.Debug("Start - Updating 'IsSyncingByNop' flag = 0");

                        if (oIntDBDataAccess.UpdateSyncingByNopFlag("Order", 0))
                        {
                            IsSyncingByNop = false;
                        }
                    }

                    _logger.Debug("End of orders sync iteration...");
                    _logger.Debug("...");
                }
                catch (Exception ex)
                {
                    _logger.Error("Exception in DBSyncProcess.SyncOrders() : " + ex);
                }
                finally
                {
                    if (IsSyncingByNop)
                    {
                        oIntDBDataAccess.UpdateSyncingByNopFlag("Order", 0);
                    }
                }

                if (ThreadSleep)
                {
                    Thread.Sleep(OrderThreadSleep);
                }
            }
        }

        private void SyncProducts(object obj)
        {
            SyncProductsProcess oSyncProductsProcess = new SyncProductsProcess();

            while (true)
            {
                try
                {
                    _logger.Debug("...");
                    _logger.Debug("Starting products sync iteration...");
                    
                    _logger.Debug("Start - Updating 'IsSyncingByNop' flag = 1");

                    if (oIntDBDataAccess.UpdateSyncingByNopFlag("Product", 1))
                    {
                        IsSyncingByNop = true;

                        oSyncProductsProcess.StartProductsSync();

                        _logger.Debug("Start - Updating 'IsSyncingByNop' flag = 0");

                        if (oIntDBDataAccess.UpdateSyncingByNopFlag("Product", 0))
                        {
                            IsSyncingByNop = false;
                        }
                    }

                    _logger.Debug("End of products sync iteration...");
                    _logger.Debug("...");
                }
                catch (Exception ex)
                {
                    _logger.Error("Exception in DBSyncProcess.SyncProducts() : " + ex);
                }
                finally
                {
                    if (IsSyncingByNop)
                    {
                        oIntDBDataAccess.UpdateSyncingByNopFlag("Product", 0);
                    }
                }

                if (ThreadSleep)
                {
                    Thread.Sleep(ProductThreadSleep);
                }
            }
        }
        
        private void SyncOrderStatus(object obj)
        {
            SyncOrderStatusProcess oSyncOrderStatusProcess = new SyncOrderStatusProcess();

            while (true)
            {
                try
                {
                    _logger.Debug("...");
                    _logger.Debug("Starting order status sync iteration...");

                    _logger.Debug("Start - Updating 'IsSyncingByNop' flag = 1");

                    if (oIntDBDataAccess.UpdateSyncingByNopFlag("OrderStatus", 1))
                    {
                        IsSyncingByNop = true;

                        oSyncOrderStatusProcess.StartOrderStatusSync();

                        _logger.Debug("Start - Updating 'IsSyncingByNop' flag = 0");

                        if (oIntDBDataAccess.UpdateSyncingByNopFlag("OrderStatus", 0))
                        {
                            IsSyncingByNop = false;
                        }
                    }

                    _logger.Debug("End of order status sync iteration...");
                    _logger.Debug("...");

                }
                catch (Exception ex)
                {
                    _logger.Error("Exception in DBSyncProcess.SyncOrderStatus() : " + ex);
                }
                finally
                {
                    if (IsSyncingByNop)
                    {
                        oIntDBDataAccess.UpdateSyncingByNopFlag("OrderStatus", 0);
                    }
                }

                if (ThreadSleep)
                {
                    Thread.Sleep(OrderStatusThreadSleep);
                }
            }
        }
    }
}

﻿using DBSyncService.DataAccess;
using DBSyncService.Entity;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBSyncService
{
    public class SyncOrdersProcess
    {
        private Logger _logger = LogManager.GetLogger("DBSyncService");
        private NCDataAccess oNCDataAccess = new NCDataAccess();
        private IntDBDataAccess oIntDBDataAccess = new IntDBDataAccess();

        public void StartOrdersSync()
        {
            try
            {
                OrderTVP oOrderTVP;
                OrderItemTVP oOrderItemTVP;
                bool SyncAllowed = true;

                if (oNCDataAccess.FetchOrders(out oOrderTVP) && oNCDataAccess.FetchOrderItems(out oOrderItemTVP))
                {
                    if (oOrderTVP == null || oOrderTVP.Count <= 0)
                    {
                        SyncAllowed = false;
                        _logger.Info("No orders found.");
                    }

                    if (oOrderItemTVP == null || oOrderItemTVP.Count <= 0)
                    {
                        SyncAllowed = false;
                        _logger.Info("No order-items found.");
                    }

                    if (SyncAllowed)
                    {
                        DateTime MaxUpdateTime = oOrderTVP.ToList<Order>().Max(oOrder => oOrder.UpdatedOnUTC);

                        if (oIntDBDataAccess.SyncOrderData(oOrderTVP, oOrderItemTVP, MaxUpdateTime))
                        {
                            _logger.Info("Successfully synced orders with Id(s) : [" + String.Join(",", oOrderTVP.ToList<Order>().Select(oOrder => oOrder.OrderId)) + "]");
                            _logger.Info("Successfully synced order-items with OrderId:CountOrderItems combination(s) : ["
                                + String.Join(",", oOrderItemTVP.ToList<OrderItem>()
                                    .GroupBy(oOrderItem1 => oOrderItem1.OrderId
                                        , (key, lstOrderItems) =>
                                            new
                                            {
                                                Key = key,
                                                OrderItems = lstOrderItems.ToList<OrderItem>()
                                            })
                                            .Select(oGroup => oGroup.Key + ":" + oGroup.OrderItems.Count)));
                            
                            if (oNCDataAccess.UpdateTimeStamp("Order", MaxUpdateTime))
                            {
                                _logger.Info("Successfully updated LastSync of Order to : " + MaxUpdateTime + " in NC DB.");
                            }
                            else
                            {
                                _logger.Error("Error occured while updating LastSync of Order to " + MaxUpdateTime + " in NC DB.");
                            }
                        }
                        else
                        {
                            _logger.Error("Error occured while syncing order data in intermediate DB.");
                        } 
                    }
                    else
                    {
                        _logger.Info("Order sync will not happen.");
                    }
                }
                else
                {
                    _logger.Error("Error occured while fetching order data from NC DB.");
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Exception in SyncOrdersProcess.StartOrdersSync() : " + ex);
            }
            finally
            {
                DateTime SyncCompletedOnTime = DateTime.UtcNow;

                // Update SyncCompletedOnTime in Nop DB.
                if (oNCDataAccess.UpdateTimeStamp("Order", SyncCompletedOnTime, "SyncCompletedOnTime"))
                {
                    _logger.Info("Successfully updated SyncCompletedOnTime of Key: Order in NC DB.");
                }
                else
                {
                    _logger.Error("Error occurred while updating SyncCompletedOnTime in Nop DB.");
                }

                // Update SyncCompletedOnTime in Integration DB.
                if (oIntDBDataAccess.UpdateTimeStamp("Order", SyncCompletedOnTime, "SyncCompletedOnTime"))
                {
                    _logger.Info("Successfully updated SyncCompletedOnTime of Key: Order in Integration DB.");
                }
                else
                {
                    _logger.Error("Error occurred while updating SyncCompletedOnTime in Integration DB.");
                }

            }
        }
    }
}

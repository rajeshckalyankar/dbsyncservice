﻿using DBSyncService.DataAccess;
using DBSyncService.Entity;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DBSyncService
{
    public class SyncProductsProcess
    {
        private Logger _logger = LogManager.GetLogger("DBSyncService");
        private NCDataAccess oNCDataAccess = new NCDataAccess();
        private IntDBDataAccess oIntDBDataAccess = new IntDBDataAccess();
        DateTime LastUpdateTimeIntDB, LastUpdateTimeNC;

        public void StartProductsSync()
        {
            try
            {
                ProductTVP oProductTVP;
                ProductStoreInventoryTVP oProductStoreInventoryTVP;
                bool SyncAllowed = true;

                if (oNCDataAccess.GetTimeStamp("Product", out LastUpdateTimeNC))
                {
                    if (oIntDBDataAccess.FetchProductsData(LastUpdateTimeNC, out oProductTVP, out oProductStoreInventoryTVP, out LastUpdateTimeIntDB))
                    {
                        if (oProductTVP == null || oProductTVP.Count <= 0)
                        {
                            SyncAllowed = false;
                            _logger.Info("No products found.");
                        }

                        if (oProductStoreInventoryTVP == null || oProductStoreInventoryTVP.Count <= 0)
                        {
                            SyncAllowed = false;
                            _logger.Info("No product-store-inverntories found.");
                        }

                        if (SyncAllowed)
                        {
                            if (oNCDataAccess.SyncProductData(oProductTVP, oProductStoreInventoryTVP, LastUpdateTimeIntDB))
                            {
                                _logger.Info("Successfully synced products with ItemCode(s) : [" + String.Join(",", oProductTVP.ToList<Product>().Select(oProduct => oProduct.ItemCode)) + "]");
                                _logger.Info("Successfully synced product-store-inventories with ItemCode:StoreCode combination(s) : [" + String.Join(",", oProductStoreInventoryTVP.ToList<ProductStoreInventory>().Select(oProductStoreInventory => oProductStoreInventory.ItemCode + ":" + oProductStoreInventory.StoreCode)) + "]");
                                _logger.Info("Successfully updated LastSync of Key:Product to : " + LastUpdateTimeIntDB);
                            }
                            else
                            {
                                _logger.Error("Error occured while syncing product data in NC DB.");
                            } 
                        }
                        else
                        {
                            _logger.Info("Product sync will not happen.");
                        }
                    }                    
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Exception in SyncProductsProcess.StartProductsSync() : " + ex);
            }
            finally
            {
                DateTime SyncCompletedOnTime = DateTime.UtcNow;

                // Update SyncCompletedOnTime in Nop DB.
                if (oNCDataAccess.UpdateTimeStamp("Product", SyncCompletedOnTime, "SyncCompletedOnTime"))
                {
                    _logger.Info("Successfully updated SyncCompletedOnTime of Key: Product in NC DB.");
                }
                else
                {
                    _logger.Error("Error occurred while updating SyncCompletedOnTime in Nop DB.");
                }

                // Update SyncCompletedOnTime in Integration DB.
                if (oIntDBDataAccess.UpdateTimeStamp("Product", SyncCompletedOnTime, "SyncCompletedOnTime"))
                {
                    _logger.Info("Successfully updated SyncCompletedOnTime of Key: Product in Integration DB.");
                }
                else
                {
                    _logger.Error("Error occurred while updating SyncCompletedOnTime in Integration DB.");
                }

            }
        }
    }
}

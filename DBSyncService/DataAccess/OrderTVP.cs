﻿using DBSyncService.Entity;
using Microsoft.SqlServer.Server;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBSyncService.DataAccess
{
    public class OrderTVP : List<Order>, IEnumerable<SqlDataRecord>
    {

        IEnumerator<SqlDataRecord> IEnumerable<SqlDataRecord>.GetEnumerator()
        {
            SqlMetaData OrderId = new SqlMetaData("OrderId", SqlDbType.Int);
            SqlMetaData CustomerOrderNumber = new SqlMetaData("CustomerOrderNumber", SqlDbType.NVarChar, -1);
            SqlMetaData OrderStatus = new SqlMetaData("OrderStatus", SqlDbType.VarChar, 20);
            SqlMetaData PaymentStatus = new SqlMetaData("PaymentStatus", SqlDbType.VarChar, 20);
            SqlMetaData ShippingMethod = new SqlMetaData("ShippingMethod", SqlDbType.NVarChar, -1);
            SqlMetaData PaymentMethod = new SqlMetaData("PaymentMethod", SqlDbType.NVarChar, -1);
            SqlMetaData StoreCode = new SqlMetaData("StoreCode", SqlDbType.VarChar, 50);
            SqlMetaData OrderTotal = new SqlMetaData("OrderTotal", SqlDbType.Decimal, 18, 4);
            SqlMetaData OrderDiscount = new SqlMetaData("OrderDiscount", SqlDbType.Decimal, 18, 4);
            SqlMetaData DeliveryCharges = new SqlMetaData("DeliveryCharges", SqlDbType.Decimal, 18, 4);
            SqlMetaData TotalAmountPaid = new SqlMetaData("TotalAmountPaid", SqlDbType.Decimal, 18, 4);
            SqlMetaData CouponCode = new SqlMetaData("CouponCode", SqlDbType.VarChar, 500);
            SqlMetaData CouponMoney = new SqlMetaData("CouponMoney", SqlDbType.Decimal, 18, 4);
            SqlMetaData RewardPointsRedeemed = new SqlMetaData("RewardPointsRedeemed", SqlDbType.Int);
            SqlMetaData RewardPointsMoney = new SqlMetaData("RewardPointsMoney", SqlDbType.Decimal, 18, 4);
            SqlMetaData FirstName = new SqlMetaData("FirstName", SqlDbType.NVarChar, -1);
            SqlMetaData LastName = new SqlMetaData("LastName", SqlDbType.NVarChar, -1);
            SqlMetaData MobileNumber = new SqlMetaData("MobileNumber", SqlDbType.NVarChar, -1);
            SqlMetaData EmailID = new SqlMetaData("EmailID", SqlDbType.NVarChar, 2000);
            SqlMetaData ShippingAddress = new SqlMetaData("ShippingAddress", SqlDbType.NVarChar, -1);
            SqlMetaData CreatedOnUTC = new SqlMetaData("CreatedOnUTC", SqlDbType.DateTime2);
            SqlMetaData UpdatedOnUTC = new SqlMetaData("UpdatedOnUTC", SqlDbType.DateTime2);

            SqlDataRecord oSqlDataRecord = new SqlDataRecord(OrderId, CustomerOrderNumber, OrderStatus, PaymentStatus, ShippingMethod, PaymentMethod, StoreCode, OrderTotal,
                OrderDiscount, DeliveryCharges, TotalAmountPaid, CouponCode, CouponMoney, RewardPointsRedeemed, RewardPointsMoney, FirstName, LastName, MobileNumber, EmailID,
                ShippingAddress, CreatedOnUTC, UpdatedOnUTC);

            foreach (Order oOrder in this)
            {
                oSqlDataRecord.SetInt32(0, oOrder.OrderId);
                oSqlDataRecord.SetString(1, oOrder.CustomOrderNumber);
                oSqlDataRecord.SetString(2, oOrder.OrderStatus.ToString());
                oSqlDataRecord.SetString(3, oOrder.PaymentStatus.ToString());

                if (oOrder.ShippingMethod != null)
                {
                    oSqlDataRecord.SetString(4, oOrder.ShippingMethod);
                }
                else
                {
                    oSqlDataRecord.SetDBNull(4);
                }

                if (oOrder.PaymentMethod != null)
                {
                    oSqlDataRecord.SetString(5, oOrder.PaymentMethod);
                }
                else
                {
                    oSqlDataRecord.SetDBNull(5);
                }

                if (oOrder.StoreCode != null)
                {
                    oSqlDataRecord.SetString(6, oOrder.StoreCode);
                }
                else
                {
                    oSqlDataRecord.SetDBNull(6);
                }

                oSqlDataRecord.SetDecimal(7, oOrder.OrderTotal);
                oSqlDataRecord.SetDecimal(8, oOrder.OrderDiscount);
                oSqlDataRecord.SetDecimal(9, oOrder.DeliveryCharges);
                oSqlDataRecord.SetDecimal(10, oOrder.TotalAmountPaid);

                if (oOrder.CouponCode != null)
                {
                    oSqlDataRecord.SetString(11, oOrder.CouponCode);
                }
                else
                {
                    oSqlDataRecord.SetDBNull(11);
                }

                if (oOrder.CouponMoney.HasValue)
                {
                    oSqlDataRecord.SetDecimal(12, oOrder.CouponMoney.Value);
                }
                else
                {
                    oSqlDataRecord.SetDBNull(12);
                }

                if (oOrder.RewardPointsRedeemed.HasValue)
                {
                    oSqlDataRecord.SetInt32(13, oOrder.RewardPointsRedeemed.Value);
                }
                else
                {
                    oSqlDataRecord.SetDBNull(13);
                }

                if (oOrder.RewardPointsMoney.HasValue)
                {
                    oSqlDataRecord.SetDecimal(14, oOrder.RewardPointsMoney.Value);
                }
                else
                {
                    oSqlDataRecord.SetDBNull(14);
                }

                if (oOrder.FirstName != null)
                {
                    oSqlDataRecord.SetString(15, oOrder.FirstName);
                }
                else
                {
                    oSqlDataRecord.SetDBNull(15);
                }

                if (oOrder.LastName != null)
                {
                    oSqlDataRecord.SetString(16, oOrder.LastName);
                }
                else
                {
                    oSqlDataRecord.SetDBNull(16);
                }

                if (oOrder.MobileNumber != null)
                {
                    oSqlDataRecord.SetString(17, oOrder.MobileNumber);
                }
                else
                {
                    oSqlDataRecord.SetDBNull(17);
                }

                if (oOrder.EmailID != null)
                {
                    oSqlDataRecord.SetString(18, oOrder.EmailID);
                }
                else
                {
                    oSqlDataRecord.SetDBNull(18);
                }
                
                if (oOrder.ShippingAddress != null)
                {
                    oSqlDataRecord.SetString(19, oOrder.ShippingAddress);
                }
                else
                {
                    oSqlDataRecord.SetDBNull(19);
                }

                oSqlDataRecord.SetSqlDateTime(20, oOrder.CreatedOnUTC);
                oSqlDataRecord.SetSqlDateTime(21, oOrder.UpdatedOnUTC);

                yield return oSqlDataRecord;
            }
        }

    }
}

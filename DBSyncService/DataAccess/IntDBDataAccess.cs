﻿using DBSyncService.Entity;
using DBSyncService.Enums;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBSyncService.DataAccess
{
    public class IntDBDataAccess
    {
        private Logger _logger = LogManager.GetLogger("DBSyncService");

        #region Application configurations

        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.AppSettings["IntDBConnectionString"];
            }
        }

        private string SQLFetchProducts
        {
            get
            {
                return ConfigurationManager.AppSettings["SQLFetchProducts"];
            }
        }

        private string SQLUpdateLastSyncedTime
        {
            get
            {
                return ConfigurationManager.AppSettings["SQLUpdateLastSyncedTime"];
            }
        }

        private string SQLUpdateSyncCompletedOnTime
        {
            get
            {
                return ConfigurationManager.AppSettings["SQLUpdateSyncCompletedOnTime"];
            }
        }

        private string SPGetProductData
        {
            get
            {
                return ConfigurationManager.AppSettings["SPGetProductData"];
            }
        }

        private string SQLFetchProductStoreInventories
        {
            get
            {
                return ConfigurationManager.AppSettings["SQLFetchProductStoreInventories"];
            }
        }

        private string SPSyncOrderData
        {
            get
            {
                return ConfigurationManager.AppSettings["SPSyncOrderData"];
            }
        }

        private string SQLGetTimeStamp
        {
            get
            {
                return ConfigurationManager.AppSettings["SQLGetTimeStamp"];
            }
        }

        private string SPGetOrderStatusData
        {
            get
            {
                return ConfigurationManager.AppSettings["SPGetOrderStatusData"];
            }
        }

        private string SPSyncOrderStatusData
        {
            get
            {
                return ConfigurationManager.AppSettings["SPSyncOrderStatusData"];
            }
        }

        private string SPUpdateSyncingByNopFlag
        {
            get
            {
                return ConfigurationManager.AppSettings["SPUpdateSyncingByNopFlag"];
            }
        }

        internal bool UpdateOrderStatusSyncResult(DataTable dtblOrderStatusSyncResult)
        {
            bool bReturn = false;

            try
            {
                using (SqlConnection oSqlConnection = new SqlConnection(ConnectionString))
                {
                    oSqlConnection.Open();

                    using (SqlCommand oSqlCommand = new SqlCommand(SPSyncOrderStatusData, oSqlConnection))
                    {
                        oSqlCommand.CommandType = CommandType.StoredProcedure;

                        oSqlCommand.Parameters.Add("@pOutput", SqlDbType.Int).Direction = ParameterDirection.Output;
                        oSqlCommand.Parameters.Add("@pOrderStatusTVP", SqlDbType.Structured).Value = dtblOrderStatusSyncResult;

                        oSqlCommand.ExecuteNonQuery();
                        
                        int SPExecutionResult = Convert.ToInt32(oSqlCommand.Parameters["@pOutput"].Value);

                        if (SPExecutionResult == 0)
                        {
                            bReturn = true;
                            _logger.Debug("Successfully executed SP : " + SPSyncOrderStatusData);
                            _logger.Debug("Successfully synced order sync status  in intermediate DB.");
                        }                        
                        else
                        {
                            _logger.Debug("Error occurred while executing SP : " + SPSyncOrderStatusData);
                            _logger.Error("Error occured while syncing order sync status in intermediate DB.");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Debug("Error occured while syncing order data.");
                _logger.Error("Exception in IntDBDataAccess.SyncOrderData() : " + ex);
            }
            return bReturn;
        }

        #endregion

        /// <summary>
        /// This method updates either 'LastSyncedTime' or 'SyncCompletedOnTime' depends on parameter 'SyncTimeKey'.
        /// </summary>
        /// <param name="Key"></param>
        /// <param name="SyncTime">LastSyncedTime or SyncCompletedOnTime</param>
        /// <param name="SyncTimeKey">which column need to sync: LastSyncedTime or SyncCompletedOnTime</param>
        /// <returns>1 or 0</returns>
        internal bool UpdateTimeStamp(string Key, DateTime SyncTime, string SyncTimeKey = "LastSyncedTime")
        {
            bool bReturn = false;
            string SQLUpdateSynceTime;
            try
            {
                SQLUpdateSynceTime = (SyncTimeKey == "LastSyncedTime") ? SQLUpdateLastSyncedTime : SQLUpdateSyncCompletedOnTime;
                using (SqlConnection oSqlConnection = new SqlConnection(ConnectionString))
                {
                    oSqlConnection.Open();

                    using (SqlCommand oSqlCommand = new SqlCommand(SQLUpdateSynceTime, oSqlConnection))
                    {
                        oSqlCommand.Parameters.Add("@Key", SqlDbType.VarChar, 20).Value = Key;
                        oSqlCommand.Parameters.Add("@SyncTime", SqlDbType.DateTime).Value = SyncTime;

                        if (oSqlCommand.ExecuteNonQuery() == 1)
                        {
                            _logger.Debug("Successfully updated " + SyncTimeKey + " of " + Key + " to " + SyncTime);
                            bReturn = true;
                        }
                        else
                        {
                            _logger.Debug("Error occurred while updating " + SyncTimeKey + " of " + Key + " to " + SyncTime);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred while updating LastSyncedTime of " + Key);
                _logger.Error("Error occurred while updating SyncCompletedOnTime of " + Key);
                _logger.Error("Exception in IntDBDataAccess.UpdateTimeStamp() : " + ex);
            }
            return bReturn;
        }

        //internal bool FetchProducts(out ProductTVP oProductTVP)
        //{
        //    bool bReturn = false;
        //    oProductTVP = null;
        //    try
        //    {
        //        using (SqlConnection oSqlConnection = new SqlConnection(ConnectionString))
        //        {
        //            using (SqlCommand oSqlCommand = new SqlCommand(SQLFetchProducts, oSqlConnection))
        //            {
        //                using (SqlDataReader oSqlDataReader = oSqlCommand.ExecuteReader())
        //                {
        //                    oProductTVP = new ProductTVP();

        //                    while (oSqlDataReader.Read())
        //                    {
        //                        Product oProduct = new Product();

        //                        oProduct.CreatedOnUTC = Convert.ToDateTime(oSqlDataReader["CreatedOnUTC"]);
        //                        oProduct.DiscountedPrice = Convert.ToDecimal(oSqlDataReader["DiscountedPrice"]);
        //                        oProduct.ItemCode = oSqlDataReader["ItemCode"].ToString();
        //                        oProduct.Price = Convert.ToDecimal(oSqlDataReader["Price"]);
        //                        oProduct.StockQuantity = Convert.ToInt32(oSqlDataReader["StockQuantity"]);
        //                        oProduct.UpdatedOnUTC = Convert.ToDateTime(oSqlDataReader["UpdatedOnUTC"]);

        //                        oProductTVP.Add(oProduct);
        //                    }
        //                    _logger.Debug("Successfully fetched Product records.");
        //                    bReturn = true;
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Debug("Error occured while fetching Product records.");
        //        _logger.Error("Exception in IntDBDataAccess.FetchProducts() : " + ex);
        //    }
        //    return bReturn;
        //}

        //internal bool FetchProductStoreInventories(out ProductStoreInventoryTVP oProductStoreInventoryTVP)
        //{
        //    bool bReturn = false;
        //    oProductStoreInventoryTVP = null;
        //    try
        //    {
        //        using (SqlConnection oSqlConnection = new SqlConnection(ConnectionString))
        //        {
        //            using (SqlCommand oSqlCommand = new SqlCommand(SQLFetchProductStoreInventories, oSqlConnection))
        //            {
        //                using (SqlDataReader oSqlDataReader = oSqlCommand.ExecuteReader())
        //                {
        //                    oProductStoreInventoryTVP = new ProductStoreInventoryTVP();

        //                    while (oSqlDataReader.Read())
        //                    {
        //                        ProductStoreInventory oProductStoreInventory = new ProductStoreInventory();

        //                        oProductStoreInventory.CreatedOnUTC = Convert.ToDateTime(oSqlDataReader["CreatedOnUTC"]);
        //                        oProductStoreInventory.ItemCode = oSqlDataReader["ItemCode"].ToString();
        //                        oProductStoreInventory.ReservedQuantity = Convert.ToInt32(oSqlDataReader["ReservedQuantity"]);
        //                        oProductStoreInventory.StockQuantity = Convert.ToInt32(oSqlDataReader["StockQuantity"]);
        //                        oProductStoreInventory.StoreCode = oSqlDataReader["StoreCode"].ToString();
        //                        oProductStoreInventory.UpdatedOnUTC = Convert.ToDateTime(oSqlDataReader["UpdatedOnUTC"]);

        //                        oProductStoreInventoryTVP.Add(oProductStoreInventory);
        //                    }
        //                    _logger.Debug("Successfully fetched ProductStoreInventory records.");
        //                    bReturn = true;
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Debug("Error occured while fetching ProductStoreInventory records.");
        //        _logger.Error("Exception in IntDBDataAccess.FetchProductStoreInventories() : " + ex);
        //    }
        //    return bReturn;
        //}

        //internal bool UpdateTimeStamp(string Key, DateTime LastSync)
        //{
        //    bool bReturn = false;

        //    try
        //    {
        //        using (SqlConnection oSqlConnection = new SqlConnection(ConnectionString))
        //        {
        //            using (SqlCommand oSqlCommand = new SqlCommand(SQLUpdateSyncTime, oSqlConnection))
        //            {
        //                oSqlCommand.Parameters.Add("@Key", SqlDbType.VarChar, 20).Value = Key;
        //                oSqlCommand.Parameters.Add("@LastSync", SqlDbType.DateTime).Value = LastSync;

        //                if (oSqlCommand.ExecuteNonQuery() == 1)
        //                {
        //                    _logger.Debug("Successfully updated last sync time of " + Key);
        //                    bReturn = true;
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        _logger.Debug("Error occured while updating last sync time of " + Key);
        //        _logger.Error("Exception in IntDBDataAccess.UpdateTimeStamp() : " + ex);
        //    }
        //    return bReturn;
        //}

        internal bool UpdateSyncingByNopFlag(string Key, int Flag)
        {
            try
            {
                using (SqlConnection oSqlConn = new SqlConnection(ConnectionString))
                {
                    oSqlConn.Open();

                    //START
                    using (SqlCommand oSqlCommandCheck = new SqlCommand(SPUpdateSyncingByNopFlag, oSqlConn))
                    {
                        oSqlCommandCheck.CommandType = CommandType.StoredProcedure;

                        oSqlCommandCheck.Parameters.Add("@pKey", SqlDbType.VarChar).Value = Key;
                        oSqlCommandCheck.Parameters.Add("@pFlag", SqlDbType.Bit).Value = Flag;
                        oSqlCommandCheck.Parameters.Add("@pIsSyncingByNopFlagUpdated", SqlDbType.Int).Direction = ParameterDirection.Output;

                        oSqlCommandCheck.ExecuteNonQuery();

                        _logger.Debug("Successfully executed SP : " + SPUpdateSyncingByNopFlag);

                        int SPExecutionResult = Convert.ToInt32(oSqlCommandCheck.Parameters["@pIsSyncingByNopFlagUpdated"].Value);

                        if (SPExecutionResult == 0)
                        {
                            _logger.Debug("IsSyncingByNop flag is NOT updated, as syncing by Almailem is in progress.");
                            return false;
                        }
                        else if (SPExecutionResult == 1)
                        {
                            _logger.Debug("End - Updated successfully 'IsSyncingByNop' flag = " + Convert.ToString(Flag));
                            return true;
                        }
                        else
                        {
                            _logger.Debug("IsSyncingByNop flag is NOT updated.");
                            return false;
                        }
                    }
                    //END
                                        
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error occured while UpdateSyncingByNopFlag.");
                _logger.Error("Exception in IntDBDataAccess.UpdateSyncingByNopFlag() : " + ex);
                return false;
            }
        }

        internal bool FetchProductsData(DateTime LastUpdateTimeNC, out ProductTVP oProductTVP, out ProductStoreInventoryTVP oProductStoreInventoryTVP, out DateTime LastUpdateTimeIntDB)
        {
            bool bReturn = false;
            oProductTVP = null;
            oProductStoreInventoryTVP = null;
            LastUpdateTimeIntDB = DateTime.MinValue;

            try
            {
                using (SqlConnection oSqlConnection = new SqlConnection(ConnectionString))
                {
                    oSqlConnection.Open();

                    using (SqlCommand oSqlCommand = new SqlCommand(SPGetProductData, oSqlConnection))
                    {
                        oSqlCommand.CommandType = CommandType.StoredProcedure;

                        oSqlCommand.Parameters.Add("@pLastUpdateTimeNC", SqlDbType.DateTime2).Value = LastUpdateTimeNC;

                        using (SqlDataReader oSqlDataReader = oSqlCommand.ExecuteReader())
                        {
                            if (oSqlDataReader.HasRows)
                            {
                                _logger.Debug("Successfully executed SP : " + SPGetProductData);

                                while (oSqlDataReader.Read())
                                {
                                    LastUpdateTimeIntDB = Convert.ToDateTime(oSqlDataReader["LastSync"]);
                                }

                                _logger.Debug("Successfully read last update time of products. Time : " + LastUpdateTimeIntDB);

                                if (LastUpdateTimeIntDB > LastUpdateTimeNC)
                                {
                                    oSqlDataReader.NextResult();

                                    #region Read Products

                                    oProductTVP = new ProductTVP();

                                    while (oSqlDataReader.Read())
                                    {
                                        Product oProduct = new Product();

                                        //NOT NULL
                                        oProduct.CreatedOnUTC = Convert.ToDateTime(oSqlDataReader["CreatedOnUTC"]);
                                        oProduct.DiscountedPrice = Convert.ToDecimal(oSqlDataReader["DiscountedPrice"]);
                                        oProduct.Price = Convert.ToDecimal(oSqlDataReader["Price"]);
                                        oProduct.StockQuantity = Convert.ToInt32(oSqlDataReader["StockQuantity"]);
                                        oProduct.UpdatedOnUTC = Convert.ToDateTime(oSqlDataReader["UpdatedOnUTC"]);

                                        //NULLABLE
                                        if (oSqlDataReader["ItemCode"] != DBNull.Value)
                                        {
                                            oProduct.ItemCode = oSqlDataReader["ItemCode"].ToString();
                                        }

                                        oProductTVP.Add(oProduct);
                                    }
                                    _logger.Debug("Successfully read products. Count : " + oProductTVP.Count);

                                    #endregion

                                    oSqlDataReader.NextResult();

                                    #region Read ProductStoreInventory

                                    oProductStoreInventoryTVP = new ProductStoreInventoryTVP();

                                    while (oSqlDataReader.Read())
                                    {
                                        ProductStoreInventory oProductStoreInventory = new ProductStoreInventory();

                                        //NOT NULL
                                        oProductStoreInventory.CreatedOnUTC = Convert.ToDateTime(oSqlDataReader["CreatedOnUTC"]);
                                        oProductStoreInventory.ReservedQuantity = Convert.ToInt32(oSqlDataReader["ReservedQuantity"]);
                                        oProductStoreInventory.StockQuantity = Convert.ToInt32(oSqlDataReader["StockQuantity"]);
                                        oProductStoreInventory.StoreCode = oSqlDataReader["StoreCode"].ToString();
                                        oProductStoreInventory.UpdatedOnUTC = Convert.ToDateTime(oSqlDataReader["UpdatedOnUTC"]);

                                        //NULLABLE
                                        if (oSqlDataReader["ItemCode"] != DBNull.Value)
                                        {
                                            oProductStoreInventory.ItemCode = oSqlDataReader["ItemCode"].ToString();
                                        }

                                        oProductStoreInventoryTVP.Add(oProductStoreInventory);
                                    }
                                    _logger.Debug("Successfully read product-store-inverntories. Count : " + oProductStoreInventoryTVP.Count);

                                    #endregion

                                    bReturn = true;
                                }
                                else
                                {
                                    _logger.Info("No updated product data found.");
                                }
                            }
                            else
                            {
                                _logger.Error("Error occured in intermediate DB while fetching product data.");
                                //_logger.Error("SyncTimestamp key not found in DB while fetching product data.");
                            }
                        }
                        return bReturn;
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error occured while fetching products data.");
                _logger.Error("Exception in IntDBDataAccess.FetchProductsData() : " + ex);
                return bReturn;
            }
            
        }

        internal bool SyncOrderData(OrderTVP oOrderTVP, OrderItemTVP oOrderItemTVP, DateTime MaxUpdateTime)
        {
            bool bReturn = false;

            try
            {
                using (SqlConnection oSqlConnection = new SqlConnection(ConnectionString))
                {
                    oSqlConnection.Open();

                    using (SqlCommand oSqlCommand = new SqlCommand(SPSyncOrderData, oSqlConnection))
                    {
                        oSqlCommand.CommandType = CommandType.StoredProcedure;

                        oSqlCommand.Parameters.Add("@pOutput", SqlDbType.Int).Direction = ParameterDirection.Output;
                        oSqlCommand.Parameters.Add("@pOrderTVP", SqlDbType.Structured).Value = oOrderTVP;
                        oSqlCommand.Parameters.Add("@pOrderItemTVP", SqlDbType.Structured).Value = oOrderItemTVP;
                        oSqlCommand.Parameters.Add("@pMaxUpdateTime", SqlDbType.DateTime2).Value = MaxUpdateTime;

                        oSqlCommand.ExecuteNonQuery();

                        //if (oSqlCommand.ExecuteNonQuery() != -1)
                        //{
                        //    _logger.Debug("Successfully executed SP : " + SPSyncOrderData);

                        int SPExecutionResult = Convert.ToInt32(oSqlCommand.Parameters["@pOutput"].Value);

                        if (SPExecutionResult == 0)
                        {
                            bReturn = true;
                            _logger.Debug("Successfully executed SP : " + SPSyncOrderData);
                            _logger.Debug("Successfully synced orders in intermediate DB.");
                        }
                        else if (SPExecutionResult == 1)
                        {
                            _logger.Debug("Error occurred while syncing orders in intermediate DB. Could not sync some orders from OrderTVP.");
                        }
                        else if (SPExecutionResult == 2)
                        {
                            _logger.Debug("Error occurred while syncing orders in intermediate DB. Could not sync some order items from OrderItemTVP.");
                        }
                        else if (SPExecutionResult == 3)
                        {
                            _logger.Debug("Error occurred while syncing orders in intermediate DB. Could not find last sync for key = order.");
                        }
                        else
                        {
                            _logger.Debug("Error occurred while executing SP : " + SPSyncOrderData);
                            _logger.Error("Error occured while syncing orders in intermediate DB.");
                        }
                        //}
                        //else
                        //{
                        //    _logger.Debug("Error occurred while executing SP : " + SPSyncOrderData);
                        //    _logger.Error("Error occured while syncing orders in intermediate DB.");
                        //} 
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Debug("Error occured while syncing order data.");
                _logger.Error("Exception in IntDBDataAccess.SyncOrderData() : " + ex);
            }
            return bReturn;
        }

        internal bool FetchOrderStatusData(DateTime LastMinUpdateTimeIntDB, out List<OrderStatusSync> lstOrderStatusSync)
        {
            bool bReturn = false;
            lstOrderStatusSync = null;

            try
            {
                using (SqlConnection oSqlConnection = new SqlConnection(ConnectionString))
                {
                    oSqlConnection.Open();

                    using (SqlCommand oSqlCommand = new SqlCommand(SPGetOrderStatusData, oSqlConnection))
                    {
                        oSqlCommand.CommandType = CommandType.StoredProcedure;

                        //oSqlCommand.Parameters.Add("@pLastMinUpdateTimeIntDB", SqlDbType.DateTime2).Value = LastMinUpdateTimeIntDB;

                        using (SqlDataReader oSqlDataReader = oSqlCommand.ExecuteReader())
                        {
                            _logger.Debug("Successfully executed SP : " + SPGetProductData);

                            if (oSqlDataReader.HasRows)
                            {     
                                lstOrderStatusSync = new List<OrderStatusSync>();

                                while (oSqlDataReader.Read())
                                {
                                    OrderStatusSync oOrderStatusSync = new OrderStatusSync();

                                    oOrderStatusSync.CustomOrderNumber = oSqlDataReader["CustomOrderNumber"].ToString();
                                    oOrderStatusSync.OrderId = Convert.ToInt32(oSqlDataReader["OrderId"]);
                                    oOrderStatusSync.OrderStatus = Convert.ToString(oSqlDataReader["OrderStatus"]);
                                    oOrderStatusSync.PaymentStatus = Convert.ToString(oSqlDataReader["PaymentStatus"].ToString());
                                    oOrderStatusSync.UpdatedOnUTC = Convert.ToDateTime(oSqlDataReader["UpdatedOnUTC"]);
                                    oOrderStatusSync.OrderStatusSyncStatus = -1;

                                    //oOrderStatusSync.OrderStatus = (OrderStatus)Enum.Parse(typeof(OrderStatus), oSqlDataReader["OrderStatus"].ToString());
                                    //oOrderStatusSync.PaymentStatus = (PaymentStatus)Enum.Parse(typeof(PaymentStatus), oSqlDataReader["PaymentStatus"].ToString());

                                    lstOrderStatusSync.Add(oOrderStatusSync);
                                }

                                _logger.Debug("Successfully read OrderStatus data. Count : " + lstOrderStatusSync.Count);
                                
                                bReturn = true;
                            }
                            else
                            {
                                bReturn = true;
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error occured while fetching OrderStatus data.");
                _logger.Error("Exception in IntDBDataAccess.FetchOrderStatusData() : " + ex);
            }
            return bReturn;
        }

        internal bool GetTimeStamp(string Key, out DateTime LastUpdateTime)
        {
            bool bReturn = false;
            LastUpdateTime = DateTime.MinValue;

            try
            {
                using (SqlConnection oSqlConnection = new SqlConnection(ConnectionString))
                {
                    oSqlConnection.Open();

                    using (SqlCommand oSqlCommand = new SqlCommand(SQLGetTimeStamp, oSqlConnection))
                    {
                        oSqlCommand.Parameters.Add("@Key", SqlDbType.VarChar, 20).Value = Key;

                        var Result = oSqlCommand.ExecuteScalar();
                        if (Result != null && Result != DBNull.Value)
                        {
                            LastUpdateTime = Convert.ToDateTime(Result);
                            bReturn = true;
                            _logger.Debug("Successfully fetched last sync time for key = " + Key + " from Int DB. Last sync is : " + LastUpdateTime);
                        }
                        else
                        {
                            _logger.Error("Error occured while fetching last sync for key = " + Key);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error occured while fetching last sync time of " + Key);
                _logger.Error("Exception in IntDBDataAccess.GetTimeStamp() : " + ex);
            }
            return bReturn;
        }
    }
}

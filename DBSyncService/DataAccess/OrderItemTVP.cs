﻿using DBSyncService.Entity;
using Microsoft.SqlServer.Server;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBSyncService.DataAccess
{
    public class OrderItemTVP : List<OrderItem>, IEnumerable<SqlDataRecord>
    {
        IEnumerator<SqlDataRecord> IEnumerable<SqlDataRecord>.GetEnumerator()
        {
            SqlMetaData OrderId = new SqlMetaData("OrderId", SqlDbType.Int);
            SqlMetaData ItemCode = new SqlMetaData("ItemCode", SqlDbType.NVarChar, 400);
            SqlMetaData Quantity = new SqlMetaData("Quantity", SqlDbType.Int);
            SqlMetaData UnitPrice = new SqlMetaData("UnitPrice", SqlDbType.Decimal, 18, 4);
            SqlMetaData TotalPrice = new SqlMetaData("TotalPrice", SqlDbType.Decimal, 18, 4);
            SqlMetaData DiscountPrice = new SqlMetaData("DiscountPrice", SqlDbType.Decimal, 18, 4);
            SqlMetaData FinalPrice = new SqlMetaData("FinalPrice", SqlDbType.Decimal, 18, 4);
            SqlMetaData CreatedOnUTC = new SqlMetaData("CreatedOnUTC", SqlDbType.DateTime2);
            SqlMetaData UpdatedOnUTC = new SqlMetaData("UpdatedOnUTC", SqlDbType.DateTime2);

            SqlDataRecord oSqlDataRecord = new SqlDataRecord(OrderId, ItemCode, Quantity, UnitPrice, TotalPrice, DiscountPrice, FinalPrice, CreatedOnUTC, UpdatedOnUTC);

            foreach (OrderItem oOrderItem in this)
            {
                oSqlDataRecord.SetInt32(0, oOrderItem.OrderId);
                oSqlDataRecord.SetString(1, oOrderItem.ItemCode);
                oSqlDataRecord.SetInt32(2, oOrderItem.Quantity);
                oSqlDataRecord.SetDecimal(3, oOrderItem.UnitPrice);
                oSqlDataRecord.SetDecimal(4, oOrderItem.TotalPrice);
                oSqlDataRecord.SetDecimal(5, oOrderItem.DiscountPrice);
                oSqlDataRecord.SetDecimal(6, oOrderItem.FinalPrice);
                oSqlDataRecord.SetSqlDateTime(7, oOrderItem.CreatedOnUTC);
                oSqlDataRecord.SetSqlDateTime(8, oOrderItem.UpdatedOnUTC);

                yield return oSqlDataRecord;
            }
        }
    }
}

﻿using DBSyncService.Entity;
using Microsoft.SqlServer.Server;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBSyncService.DataAccess
{
    public class ProductTVP : List<Product>, IEnumerable<SqlDataRecord>
    {

        IEnumerator<SqlDataRecord> IEnumerable<SqlDataRecord>.GetEnumerator()
        {
            SqlMetaData ItemCode = new SqlMetaData("ItemCode", SqlDbType.NVarChar, 400);
            SqlMetaData StockQuantity = new SqlMetaData("StockQuantity", SqlDbType.Int);
            SqlMetaData Price = new SqlMetaData("Price", SqlDbType.Decimal, 18, 4);
            SqlMetaData DiscountedPrice = new SqlMetaData("DiscountedPrice", SqlDbType.Decimal, 18, 4);
            SqlMetaData CreatedOnUTC = new SqlMetaData("CreatedOnUTC", SqlDbType.DateTime2);
            SqlMetaData UpdatedOnUTC = new SqlMetaData("UpdatedOnUTC", SqlDbType.DateTime2);

            SqlDataRecord oSqlDataRecord = new SqlDataRecord(ItemCode, StockQuantity, Price, DiscountedPrice, CreatedOnUTC, UpdatedOnUTC);

            foreach(Product oProduct in this)
            {
                oSqlDataRecord.SetString(0, oProduct.ItemCode);
                oSqlDataRecord.SetInt32(1, oProduct.StockQuantity);
                oSqlDataRecord.SetDecimal(2, oProduct.Price);
                oSqlDataRecord.SetDecimal(3, oProduct.DiscountedPrice);
                oSqlDataRecord.SetSqlDateTime(4, oProduct.CreatedOnUTC);
                oSqlDataRecord.SetSqlDateTime(5, oProduct.UpdatedOnUTC);

                yield return oSqlDataRecord;
            }
        }
    }
}

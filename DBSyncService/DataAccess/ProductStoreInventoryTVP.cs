﻿using DBSyncService.Entity;
using Microsoft.SqlServer.Server;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBSyncService.DataAccess
{
    public class ProductStoreInventoryTVP:List<ProductStoreInventory>,IEnumerable<SqlDataRecord>
    {

        IEnumerator<SqlDataRecord> IEnumerable<SqlDataRecord>.GetEnumerator()
        {
            SqlMetaData ItemCode = new SqlMetaData("ItemCode", SqlDbType.NVarChar, 400);
            SqlMetaData StoreCode = new SqlMetaData("StoreCode", SqlDbType.VarChar, 50);
            SqlMetaData StockQuantity = new SqlMetaData("StockQuantity", SqlDbType.Int);
            SqlMetaData ReservedQuantity = new SqlMetaData("ReservedQuantity", SqlDbType.Int);
            SqlMetaData CreatedOnUTC = new SqlMetaData("CreatedOnUTC", SqlDbType.DateTime2);
            SqlMetaData UpdatedOnUTC = new SqlMetaData("UpdatedOnUTC", SqlDbType.DateTime2);

            SqlDataRecord oSqlDataRecord = new SqlDataRecord(ItemCode, StoreCode, StockQuantity, ReservedQuantity, CreatedOnUTC, UpdatedOnUTC);

            foreach(ProductStoreInventory oProductStoreInventory in this)
            {
                oSqlDataRecord.SetString(0, oProductStoreInventory.ItemCode);
                oSqlDataRecord.SetString(1, oProductStoreInventory.StoreCode);
                oSqlDataRecord.SetInt32(2, oProductStoreInventory.StockQuantity);
                oSqlDataRecord.SetInt32(3, oProductStoreInventory.ReservedQuantity);
                oSqlDataRecord.SetSqlDateTime(4, oProductStoreInventory.CreatedOnUTC);
                oSqlDataRecord.SetSqlDateTime(5, oProductStoreInventory.UpdatedOnUTC);

                yield return oSqlDataRecord;
            }
        }
    }
}

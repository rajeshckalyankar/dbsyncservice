﻿using DBSyncService.Entity;
using DBSyncService.Enums;
using NLog;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBSyncService.DataAccess
{
    public class NCDataAccess
    {
        private Logger _logger = LogManager.GetLogger("DBSyncService");

        #region Application configurations

        private string ConnectionString
        {
            get
            {
                return ConfigurationManager.AppSettings["NCConnectionString"];
            }
        }

        private string SQLFetchOrders
        {
            get
            {
                return ConfigurationManager.AppSettings["SQLFetchOrder"];
            }
        }

        private string SQLFetchOrderItems
        {
            get
            {
                return ConfigurationManager.AppSettings["SQLFetchOrderItems"];
            }
        }

        private string SQLUpdateLastSyncedTime
        {
            get
            {
                return ConfigurationManager.AppSettings["SQLUpdateLastSyncedTime"];
            }
        }

        private string SQLUpdateSyncCompletedOnTime
        {
            get
            {
                return ConfigurationManager.AppSettings["SQLUpdateSyncCompletedOnTime"];
            }
        }

        private string SPSyncProductData
        {
            get
            {
                return ConfigurationManager.AppSettings["SPSyncProductData"];
            }
        }

        private string SQLGetTimeStamp
        {
            get
            {
                return ConfigurationManager.AppSettings["SQLGetTimeStamp"];
            }
        }

        #endregion

        internal bool FetchOrders(out OrderTVP oOrderTVP)
        {
            bool bReturn = false;
            oOrderTVP = null;
            try
            {
                using (SqlConnection oSqlConnection = new SqlConnection(ConnectionString))
                {
                    oSqlConnection.Open();

                    using (SqlCommand oSqlCommand = new SqlCommand(SQLFetchOrders, oSqlConnection))
                    {
                        using (SqlDataReader oSqlDataReader = oSqlCommand.ExecuteReader())
                        {
                            oOrderTVP = new OrderTVP();

                            while (oSqlDataReader.Read())
                            {
                                Order oOrder = new Order();

                                //NOT NULL
                                oOrder.CreatedOnUTC = Convert.ToDateTime(oSqlDataReader["CreatedOnUTC"]);
                                oOrder.CustomOrderNumber = oSqlDataReader["CustomOrderNumber"].ToString();
                                oOrder.DeliveryCharges = Convert.ToDecimal(oSqlDataReader["DeliveryCharges"]);
                                oOrder.OrderDiscount = Convert.ToDecimal(oSqlDataReader["OrderDiscount"]);
                                oOrder.OrderId = Convert.ToInt32(oSqlDataReader["OrderId"]);
                                oOrder.OrderStatus = (OrderStatus)Convert.ToInt32(oSqlDataReader["OrderStatus"]);
                                oOrder.OrderTotal = Convert.ToDecimal(oSqlDataReader["OrderTotal"]);
                                oOrder.PaymentStatus = (PaymentStatus)Convert.ToInt32(oSqlDataReader["PaymentStatus"]);
                                oOrder.TotalAmountPaid = Convert.ToDecimal(oSqlDataReader["TotalAmountPaid"]);
                                oOrder.UpdatedOnUTC = Convert.ToDateTime(oSqlDataReader["UpdatedOnUTC"]);


                                //NULLABLE
                                if (oSqlDataReader["CouponCode"] != DBNull.Value)
                                {
                                    oOrder.CouponCode = oSqlDataReader["CouponCode"].ToString();
                                }
                                if (oSqlDataReader["CouponMoney"] != DBNull.Value)
                                {
                                    oOrder.CouponMoney = Convert.ToDecimal(oSqlDataReader["CouponMoney"]);
                                }
                                if (oSqlDataReader["EmailID"] != DBNull.Value)
                                {
                                    oOrder.EmailID = oSqlDataReader["EmailID"].ToString();
                                }
                                if (oSqlDataReader["FirstName"] != DBNull.Value)
                                {
                                    oOrder.FirstName = oSqlDataReader["FirstName"].ToString();
                                }
                                if (oSqlDataReader["LastName"] != DBNull.Value)
                                {
                                    oOrder.LastName = oSqlDataReader["LastName"].ToString();
                                }
                                if (oSqlDataReader["MobileNumber"] != DBNull.Value)
                                {
                                    oOrder.MobileNumber = oSqlDataReader["MobileNumber"].ToString();
                                }
                                if (oSqlDataReader["PaymentMethod"] != DBNull.Value)
                                {
                                    oOrder.PaymentMethod = oSqlDataReader["PaymentMethod"].ToString();
                                }
                                if (oSqlDataReader["RewardPointsMoney"] != DBNull.Value)
                                {
                                    oOrder.RewardPointsMoney = Convert.ToDecimal(oSqlDataReader["RewardPointsMoney"]);
                                }
                                if (oSqlDataReader["RewardPointsRedeemed"] != DBNull.Value)
                                {
                                    oOrder.RewardPointsRedeemed = Convert.ToInt32(oSqlDataReader["RewardPointsRedeemed"]);
                                }
                                if (oSqlDataReader["ShippingAddress"] != DBNull.Value)
                                {
                                    oOrder.ShippingAddress = oSqlDataReader["ShippingAddress"].ToString();
                                }
                                if (oSqlDataReader["ShippingMethod"] != DBNull.Value)
                                {
                                    oOrder.ShippingMethod = oSqlDataReader["ShippingMethod"].ToString();
                                }
                                if (oSqlDataReader["StoreCode"] != DBNull.Value)
                                {
                                    oOrder.StoreCode = oSqlDataReader["StoreCode"].ToString();
                                }

                                oOrderTVP.Add(oOrder);
                            }
                            _logger.Debug("Successfully fetched Orders. Count : " + oOrderTVP.Count);
                            bReturn = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Debug("Error occured while fetching Order records.");
                _logger.Error("Exception in NCDataAccess.FetchOrders() : " + ex);
            }
            return bReturn;
        }

        internal bool FetchOrderItems(out OrderItemTVP oOrderItemTVP)
        {
            bool bReturn = false;
            oOrderItemTVP = null;
            try
            {
                using (SqlConnection oSqlConnection = new SqlConnection(ConnectionString))
                {
                    oSqlConnection.Open();

                    using (SqlCommand oSqlCommand = new SqlCommand(SQLFetchOrderItems, oSqlConnection))
                    {
                        using (SqlDataReader oSqlDataReader = oSqlCommand.ExecuteReader())
                        {
                            oOrderItemTVP = new OrderItemTVP();

                            while (oSqlDataReader.Read())
                            {
                                OrderItem oOrderItem = new OrderItem();

                                //NOT NULL
                                oOrderItem.CreatedOnUTC = Convert.ToDateTime(oSqlDataReader["CreatedOnUTC"]);
                                oOrderItem.DiscountPrice = Convert.ToDecimal(oSqlDataReader["DiscountPrice"]);
                                oOrderItem.OrderId = Convert.ToInt32(oSqlDataReader["OrderId"]);
                                oOrderItem.Quantity = Convert.ToInt32(oSqlDataReader["Quantity"]);
                                oOrderItem.TotalPrice = Convert.ToDecimal(oSqlDataReader["TotalPrice"]);
                                oOrderItem.UnitPrice = Convert.ToDecimal(oSqlDataReader["UnitPrice"]);
                                oOrderItem.UpdatedOnUTC = Convert.ToDateTime(oSqlDataReader["UpdatedOnUTC"]);

                                //NULLABLE
                                if (oSqlDataReader["ItemCode"] != DBNull.Value)
                                {
                                    oOrderItem.ItemCode = oSqlDataReader["ItemCode"].ToString();
                                }

                                //CALCULATED
                                oOrderItem.FinalPrice = oOrderItem.TotalPrice - oOrderItem.DiscountPrice;

                                oOrderItemTVP.Add(oOrderItem);
                            }

                            _logger.Debug("Successfully fetched OrderItems. Count : " + oOrderItemTVP.Count);
                            bReturn = true;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Debug("Error occured while fetching OrderItem records.");
                _logger.Error("Exception in NCDataAccess.FetchOrderItems() : " + ex);
            }
            return bReturn;
        }

        /// <summary>
        /// This method updates either 'LastSyncedTime' or 'SyncCompletedOnTime' depends on parameter 'SyncTimeKey'.
        /// </summary>
        /// <param name="Key"></param>
        /// <param name="SyncTime">LastSyncedTime or SyncCompletedOnTime</param>
        /// <param name="SyncTimeKey">which column need to sync: LastSyncedTime or SyncCompletedOnTime</param>
        /// <returns>1 or 0</returns>
        internal bool UpdateTimeStamp(string Key, DateTime SyncTime, string SyncTimeKey = "LastSyncedTime")
        {
            bool bReturn = false;
            string SQLUpdateSynceTime;
            try
            {
                SQLUpdateSynceTime = (SyncTimeKey == "LastSyncedTime") ? SQLUpdateLastSyncedTime : SQLUpdateSyncCompletedOnTime;
                using (SqlConnection oSqlConnection = new SqlConnection(ConnectionString))
                {
                    oSqlConnection.Open();

                    using (SqlCommand oSqlCommand = new SqlCommand(SQLUpdateSynceTime, oSqlConnection))
                    {
                        oSqlCommand.Parameters.Add("@Key", SqlDbType.VarChar, 20).Value = Key;
                        oSqlCommand.Parameters.Add("@SyncTime", SqlDbType.DateTime).Value = SyncTime;

                        if (oSqlCommand.ExecuteNonQuery() == 1)
                        {
                            _logger.Debug("Successfully updated "+ SyncTimeKey + " of " + Key + " to " + SyncTime);
                            bReturn = true;
                        }
                        else
                        {
                            _logger.Debug("Error occurred while updating " + SyncTimeKey + " of " + Key + " to " + SyncTime);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error occurred while updating LastSyncedTime of " + Key);
                _logger.Error("Error occurred while updating SyncCompletedOnTime of " + Key);
                _logger.Error("Exception in IntDBDataAccess.UpdateTimeStamp() : " + ex);
            }
            return bReturn;
        }

        internal bool SyncProductData(ProductTVP oProductTVP, ProductStoreInventoryTVP oProductStoreInventoryTVP, DateTime LastUpdateTime)
        {
            bool bReturn = false;

            try
            {
                using (SqlConnection oSqlConnection = new SqlConnection(ConnectionString))
                {
                    oSqlConnection.Open();

                    using (SqlCommand oSqlCommand = new SqlCommand(SPSyncProductData, oSqlConnection))
                    {
                        oSqlCommand.CommandType = CommandType.StoredProcedure;

                        oSqlCommand.Parameters.Add("@pOutput", SqlDbType.Int).Direction = ParameterDirection.Output;
                        oSqlCommand.Parameters.Add("@pProductTVP", SqlDbType.Structured).Value = oProductTVP;
                        oSqlCommand.Parameters.Add("@pProductStoreInventoryTVP", SqlDbType.Structured).Value = oProductStoreInventoryTVP;
                        oSqlCommand.Parameters.Add("@pLastUpdateTime", SqlDbType.DateTime2).Value = LastUpdateTime;

                        oSqlCommand.ExecuteNonQuery();

                        //if (oSqlCommand.ExecuteNonQuery() == 0)
                        //{

                            int SPExecutionResult = Convert.ToInt32(oSqlCommand.Parameters["@pOutput"].Value);

                            _logger.Debug("Successfully executed SP : " + SPSyncProductData);

                            if (SPExecutionResult == 0)
                            {
                                bReturn = true;                                
                                _logger.Debug("Successfully synced products in NC DB.");
                            }
                            else if (SPExecutionResult == 1)
                            {
                                _logger.Debug("Error occurred while syncing products in NC DB. Could not found some products in DB.");
                            }
                            else if (SPExecutionResult == 2)
                            {
                                _logger.Debug("Error occurred while syncing products in NC DB. Could not found some product-warehouse-inventories in DB.");
                            }
                            else if (SPExecutionResult == 3)
                            {
                                _logger.Debug("Error occurred while syncing products in NC DB. Could not find last sync for key = product.");
                            }
                            else if (SPExecutionResult == 4)
                            {
                                _logger.Debug("Error occurred while syncing products in NC DB. Found some duplicate records in ProductStoreInventory table.");
                            }
                            else
                            {
                                _logger.Debug("Error occurred while executing SP : " + SPSyncProductData);
                                _logger.Error("Error occured while syncing products in NC DB.");
                            }
                        //}
                        //else
                        //{
                        //    _logger.Error("Error occured while executing SP : " + SPSyncProductData);
                        //    _logger.Error("Error occured while syncing products in NC DB.");
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Debug("Error occured while syncing product data.");
                _logger.Error("Exception in NCDataAccess.SyncProductData() : " + ex);
            }
            return bReturn;
        }

        internal bool GetTimeStamp(string Key, out DateTime LastUpdateTime)
        {
            bool bReturn = false;
            LastUpdateTime = DateTime.MinValue;

            try
            {
                using (SqlConnection oSqlConnection = new SqlConnection(ConnectionString))
                {
                    oSqlConnection.Open();

                    using (SqlCommand oSqlCommand = new SqlCommand(SQLGetTimeStamp, oSqlConnection))
                    {
                        oSqlCommand.Parameters.Add("@Key", SqlDbType.VarChar, 20).Value = Key;

                        var Result = oSqlCommand.ExecuteScalar();
                        if (Result != null && Result != DBNull.Value)
                        {
                            LastUpdateTime = Convert.ToDateTime(Result);
                            bReturn = true;
                            _logger.Debug("Successfully fetched last sync time for key = " + Key + " from NC DB. Last sync is : " + LastUpdateTime);
                        }
                        else
                        {
                            _logger.Error("Error occured while fetching last sync for key = " + Key);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                _logger.Error("Error occured while fetching last sync time of " + Key);
                _logger.Error("Exception in NCDataAccess.GetTimeStamp() : " + ex);
            }
            return bReturn;
        }
    }
}

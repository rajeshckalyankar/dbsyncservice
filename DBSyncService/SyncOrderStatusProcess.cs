﻿using DBSyncService.APIClient;
using DBSyncService.DataAccess;
using DBSyncService.Entity;
using DBSyncService.Enums;
using NLog;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBSyncService
{
    public class SyncOrderStatusProcess
    {
        private Logger _logger = LogManager.GetLogger("DBSyncService");
        private NCDataAccess oNCDataAccess = new NCDataAccess();
        private IntDBDataAccess oIntDBDataAccess = new IntDBDataAccess();
        private NCAPIClient oNCAPIClient = new NCAPIClient();
        List<OrderStatusSync> lstOrderStatusSync = new List<OrderStatusSync>();
        public static List<OrderStatusSync> lstOrderStatusSyncResult = new List<OrderStatusSync>();

        internal void StartOrderStatusSync()
        {
            try
            {
                DateTime LastMinUpdateTimeIntDB = DateTime.Now;
                //DateTime LastSyncedTimeNC;
                //bool SyncAllowed = true;

                if(oIntDBDataAccess.GetTimeStamp("OrderStatus", out LastMinUpdateTimeIntDB))
                {
                    _logger.Info("Got Order Status LastSyncedTime in IntDB.");
                }

                //if (oNCDataAccess.GetTimeStamp("OrderStatus", out LastSyncedTimeNC) && oIntDBDataAccess.GetTimeStamp("OrderStatus", out LastMinUpdateTimeIntDB))
                //{
                //if (LastMinUpdateTimeIntDB > LastSyncedTimeNC)
                //{
                if (oIntDBDataAccess.FetchOrderStatusData(LastMinUpdateTimeIntDB, out lstOrderStatusSync))
                {
                    if (lstOrderStatusSync == null || lstOrderStatusSync.Count <= 0)
                    {
                        _logger.Info("No order status data found.");
                        _logger.Info("Order status sync will not happen.");
                    }
                    else
                    {
                        if (oNCAPIClient.ChangeOrderStatusAsync(lstOrderStatusSync))
                        {
                            _logger.Debug("Successfully called ChangeOrderStatus API.");

                            // Update LastSyncedTime in Nop DB.
                            if (oNCDataAccess.UpdateTimeStamp("OrderStatus", LastMinUpdateTimeIntDB, "LastSyncedTime"))
                            {
                                _logger.Info("Successfully updated LastSyncedTime of Key: OrderStatus in Nop DB.");
                            }
                            else
                            {
                                _logger.Error("Error occurred while updating LastSyncedTime in Nop DB.");
                            }

                            DataTable dtblOrderStatusSyncResult = lstOrderStatusSyncResult.ToDataTable();

                            if (oIntDBDataAccess.UpdateOrderStatusSyncResult(dtblOrderStatusSyncResult))
                            {
                                _logger.Info("Successfully updated OrderStatusSyncResult in Int DB.");
                            }
                            else
                            {
                                _logger.Error("Error occured while updating the result order status in intermediate DB.");
                            }
                        }
                        else
                        {
                            _logger.Error("Error occured in order status sync.");
                        }
                    }
                }
                //}
                //else
                //{
                //    _logger.Info("Status of any order is not updated yet.");
                //}
                //}
                //else
                //{
                //    _logger.Error("Order status sync will not happen.");
                //}
            }
            catch (Exception ex)
            {
                _logger.Error("Exception in SyncOrderStatusProcess.StartOrderStatusSync() : " + ex);
            }
            finally
            {                
                //lstOrderStatusSync = null;
                lstOrderStatusSyncResult = null;
                DateTime SyncCompletedOnTime = DateTime.UtcNow;

                // For OrderStatus thread - Need to update only 'SyncCompletedOnTime' = DateTime.Now;

                // Update SyncCompletedOnTime in Nop DB.
                if (oNCDataAccess.UpdateTimeStamp("OrderStatus", SyncCompletedOnTime, "SyncCompletedOnTime"))
                {
                   _logger.Info("Successfully updated SyncCompletedOnTime of Key: OrderStatus in NC DB.");
                }
                else
                {
                    _logger.Error("Error occurred while updating SyncCompletedOnTime in Nop DB.");
                }

                // Update SyncCompletedOnTime in Integration DB.
                if (oIntDBDataAccess.UpdateTimeStamp("OrderStatus", SyncCompletedOnTime, "SyncCompletedOnTime"))
                {
                    _logger.Info("Successfully updated SyncCompletedOnTime of Key: OrderStatus in Integration DB.");
                }
                else
                {
                    _logger.Error("Error occurred while updating SyncCompletedOnTime in Integration DB.");
                }

            }

        }
    }
       
}



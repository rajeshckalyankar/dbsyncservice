﻿namespace DBSyncService
{
    partial class DBSyncAppInstaller
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.DBSyncProcessInstaller = new System.ServiceProcess.ServiceProcessInstaller();
            this.DBSyncServiceInstaller = new System.ServiceProcess.ServiceInstaller();
            // 
            // DBSyncProcessInstaller
            // 
            this.DBSyncProcessInstaller.Account = System.ServiceProcess.ServiceAccount.LocalSystem;
            this.DBSyncProcessInstaller.Password = null;
            this.DBSyncProcessInstaller.Username = null;
            // 
            // DBSyncServiceInstaller
            // 
            this.DBSyncServiceInstaller.DisplayName = "DBSyncService";
            this.DBSyncServiceInstaller.ServiceName = "DBSyncService";
            // 
            // DBSyncAppInstaller
            // 
            this.Installers.AddRange(new System.Configuration.Install.Installer[] {
            this.DBSyncServiceInstaller,
            this.DBSyncProcessInstaller});

        }

        #endregion

        private System.ServiceProcess.ServiceProcessInstaller DBSyncProcessInstaller;
        private System.ServiceProcess.ServiceInstaller DBSyncServiceInstaller;
    }
}
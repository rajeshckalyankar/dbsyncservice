﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading.Tasks;
 
using System.Runtime.Serialization;
using System.Net;

namespace DBSyncService.APIClient
{
    public class NCAPIClient
    {
        #region Application Configurations
        
        private string NCAPIBaseURL
        {
            get
            {
                return ConfigurationManager.AppSettings["NCAPIBaseURL"].ToString();
            }
        }

        private string NCAPIToken
        {
            get
            {
                return ConfigurationManager.AppSettings["NCAPIToken"].ToString();
            }
        }

        private string NCAPIChangeOrderStatusEP
        {
            get
            {
                return ConfigurationManager.AppSettings["NCAPIChangeOrderStatusEP"].ToString();
            }
        }
        private bool UseSSL
        {
            get
            {
                return Convert.ToBoolean(ConfigurationManager.AppSettings["UseSSL"]);
            }
        }

        //private bool CheckSSL
        //{
        //    get
        //    {
        //        return Convert.ToBoolean(ConfigurationManager.AppSettings["CheckSSL"]);
        //    }
        //}
       
        //private bool UseTLS
        //{
        //    get
        //    {
        //        return Convert.ToBoolean(ConfigurationManager.AppSettings["UseTLS"]);
        //    }
        //}

        #endregion

        internal bool ChangeOrderStatusAsync(List<Entity.OrderStatusSync> lstOrderStatusSync)
        {
            //Added block to fix issue: "Could not create SSL/TLS secure channel" and support https API call.
            //if (CheckSSL == false)
            //{
            //    ServicePointManager.ServerCertificateValidationCallback = delegate { return true; };
            //}

            if (UseSSL == true)
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Ssl3 | SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            }

            //if (UseTLS == true)
            //{
            //    ServicePointManager.SecurityProtocol |= SecurityProtocolType.Tls12 | SecurityProtocolType.Tls11 | SecurityProtocolType.Tls;
            //}
            //
            using (var client = new HttpClient())
            {
                client.BaseAddress = new Uri(NCAPIBaseURL);
                client.DefaultRequestHeaders.Accept.Clear();
                client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

                //POST Method  
                client.DefaultRequestHeaders.Add("Authorization", NCAPIToken);
                HttpResponseMessage response = client.PostAsJsonAsync("/api/syncOrderStatusAPI/", lstOrderStatusSync).Result;

                if (response.IsSuccessStatusCode)
                {
                    var orderSyncStatus = response.Content.ReadAsAsync<OrderCompleteObject>().Result;
                    
                    //Pass the result back to calling class.                    
                    if (orderSyncStatus.OrderStatusSyncResult != null && orderSyncStatus.OrderStatusSyncResult.ToList().Count > 0)
                    {
                        SyncOrderStatusProcess.lstOrderStatusSyncResult = orderSyncStatus.OrderStatusSyncResult.ToList();
                        return true;
                    }
                    return false;
                }
                else
                {
                    //Console.WriteLine("Internal server Error");
                    return false;
                }
            }
            
        }

        internal class OrderSyncStatus
        {
            public string CustomOrderNumber;
            public string OrderStatusComment;
        }
       
    }
}

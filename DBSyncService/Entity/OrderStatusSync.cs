﻿using DBSyncService.APIClient;
using DBSyncService.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBSyncService.Entity
{
    public class OrderStatusSync
    {
        #region Private variables

        //private int _order_id;
        //private string _custom_order_number;
        //private string _order_status;
        //private string _payment_status;
        //private DateTime _updated_on_utc;
        ////private StatusResult _status_result;

        #endregion


        #region Public variables

        #region Public variables

        public int OrderId { get; set; }

        public string CustomOrderNumber { get; set; }

        public string OrderStatus { get; set; }

        public string PaymentStatus { get; set; }

        public DateTime UpdatedOnUTC { get; set; }

        public int OrderStatusSyncStatus { get; set; }

        #endregion

        //public int OrderId
        //{
        //    get
        //    {
        //        return _order_id;
        //    }
        //    set
        //    {
        //        _order_id = value;
        //    }
        //}

        //public string CustomOrderNumber
        //{
        //    get
        //    {
        //        return _custom_order_number;
        //    }
        //    set
        //    {
        //        _custom_order_number = value;
        //    }
        //}

        //public string OrderStatus
        //{
        //    get
        //    {
        //        return _order_status;
        //    }
        //    set
        //    {
        //        _order_status = value;
        //    }
        //}

        //public string PaymentStatus
        //{
        //    get
        //    {
        //        return _payment_status;
        //    }
        //    set
        //    {
        //        _payment_status = value;
        //    }
        //}

        //public DateTime UpdatedOnUTC
        //{
        //    get
        //    {
        //        return _updated_on_utc;
        //    }
        //    set
        //    {
        //        _updated_on_utc = value;
        //    }
        //}

        ////public StatusResult StatusResult
        ////{
        ////    get
        ////    {
        ////        return _status_result;
        ////    }
        ////    set
        ////    {
        ////        _status_result = value;
        ////    }
        ////}

        //public int OrderStatusSyncStatus
        //{
        //    get; set;
        //}

        #endregion
    }
}

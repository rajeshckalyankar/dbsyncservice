﻿using DBSyncService.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBSyncService.Entity
{
    public class Order
    {
        #region Private variables

        private int _order_id;
        private string _custom_order_number;
        private OrderStatus _order_status;
        private PaymentStatus _payment_status;
        private string _shipping_method;
        private string _payment_method;
        private string _store_code;
        private decimal _order_total;
        private decimal _order_discount;
        private decimal _delivery_charges;
        private decimal _total_amount_paid;
        private string _coupon_code;
        private Decimal? _coupon_money;
        private Int32? _reward_points_redeemed;
        private Decimal? _reward_points_money;
        private string _first_name;
        private string _last_name;
        private string _mobile_number;
        private string _email_id;
        private string _shipping_address;
        private DateTime _created_on_utc;
        private DateTime _updated_on_utc;

        #endregion

        #region Public variables

        public int OrderId
        {
            get
            {
                return _order_id;
            }
            set
            {
                _order_id = value;
            }
        }

        public string CustomOrderNumber
        {
            get
            {
                return _custom_order_number;
            }
            set
            {
                _custom_order_number = value;
            }
        }

        public OrderStatus OrderStatus
        {
            get
            {
                return _order_status;
            }
            set
            {
                _order_status = value;
            }
        }

        public PaymentStatus PaymentStatus
        {
            get
            {
                return _payment_status;
            }
            set
            {
                _payment_status = value;
            }
        }

        public string ShippingMethod
        {
            get
            {
                return _shipping_method;
            }
            set
            {
                _shipping_method = value;
            }
        }

        public string PaymentMethod
        {
            get
            {
                return _payment_method;
            }
            set
            {
                _payment_method = value;
            }
        }

        public string StoreCode
        {
            get
            {
                return _store_code;
            }
            set
            {
                _store_code = value;
            }
        }

        public decimal OrderTotal
        {
            get
            {
                return _order_total;
            }
            set
            {
                _order_total = value;
            }
        }

        public decimal OrderDiscount
        {
            get
            {
                return _order_discount;
            }
            set
            {
                _order_discount = value;
            }
        }

        public decimal DeliveryCharges
        {
            get
            {
                return _delivery_charges;
            }
            set
            {
                _delivery_charges = value;
            }
        }

        public decimal TotalAmountPaid
        {
            get
            {
                return _total_amount_paid;
            }
            set
            {
                _total_amount_paid = value;
            }
        }

        public string CouponCode
        {
            get
            {
                return _coupon_code;
            }
            set
            {
                _coupon_code = value;
            }
        }

        public Decimal? CouponMoney
        {
            get
            {
                return _coupon_money;
            }
            set
            {
                _coupon_money = value;
            }
        }

        public Int32? RewardPointsRedeemed
        {
            get
            {
                return _reward_points_redeemed;
            }
            set
            {
                _reward_points_redeemed = value;
            }
        }

        public Decimal? RewardPointsMoney
        {
            get
            {
                return _reward_points_money;
            }
            set
            {
                _reward_points_money = value;
            }
        }

        public string FirstName
        {
            get
            {
                return _first_name;
            }
            set
            {
                _first_name = value;
            }
        }

        public string LastName
        {
            get
            {
                return _last_name;
            }
            set
            {
                _last_name = value;
            }
        }

        public string MobileNumber
        {
            get
            {
                return _mobile_number;
            }
            set
            {
                _mobile_number = value;
            }
        }

        public string EmailID
        {
            get
            {
                return _email_id;
            }
            set
            {
                _email_id = value;
            }
        }

        public string ShippingAddress
        {
            get
            {
                return _shipping_address;
            }
            set
            {
                _shipping_address = value;
            }
        }

        public DateTime CreatedOnUTC
        {
            get
            {
                return _created_on_utc;
            }
            set
            {
                _created_on_utc = value;
            }
        }

        public DateTime UpdatedOnUTC
        {
            get
            {
                return _updated_on_utc;
            }
            set
            {
                _updated_on_utc = value;
            }
        }

        #endregion
    }
}

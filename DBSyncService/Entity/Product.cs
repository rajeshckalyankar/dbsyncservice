﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBSyncService.Entity
{
    public class Product
    {
        #region Private variables

        private string _item_code;
        private int _stock_quantity;
        private decimal _price;
        private decimal _discounted_price;
        private DateTime _created_on_utc;
        private DateTime _updated_on_utc;

        #endregion

        #region Public variables
        
        public string ItemCode
        {
            get
            {
                return _item_code;
            }
            set
            {
                _item_code = value;
            }
        }

        public int StockQuantity
        {
            get
            {
                return _stock_quantity;
            }
            set
            {
                _stock_quantity = value;
            }
        }

        public decimal Price
        {
            get
            {
                return _price;
            }
            set
            {
                _price = value;
            }
        }

        public decimal DiscountedPrice
        {
            get
            {
                return _discounted_price;
            }
            set
            {
                _discounted_price = value;
            }
        }

        public DateTime CreatedOnUTC
        {
            get
            {
                return _created_on_utc;
            }
            set
            {
                _created_on_utc = value;
            }
        }

        public DateTime UpdatedOnUTC
        {
            get
            {
                return _updated_on_utc;
            }
            set
            {
                _updated_on_utc = value;
            }
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBSyncService.Entity
{
    public class ProductStoreInventory
    {
        #region Private variables

        private string _item_code;
        private string _store_code;
        private int _stock_quantity;
        private int _reserved_quantity;
        private DateTime _created_on_utc;
        private DateTime _updated_on_utc;

        #endregion

        #region Public variables
        
        public string ItemCode
        {
            get
            {
                return _item_code;
            }
            set
            {
                _item_code = value;
            }
        }

        public string StoreCode
        {
            get
            {
                return _store_code;
            }
            set
            {
                _store_code = value;
            }
        }

        public int StockQuantity
        {
            get
            {
                return _stock_quantity;
            }
            set
            {
                _stock_quantity = value;
            }
        }

        public int ReservedQuantity
        {
            get
            {
                return _reserved_quantity;
            }
            set
            {
                _reserved_quantity = value;
            }
        }

        public DateTime CreatedOnUTC
        {
            get
            {
                return _created_on_utc;
            }
            set
            {
                _created_on_utc = value;
            }
        }

        public DateTime UpdatedOnUTC
        {
            get
            {
                return _updated_on_utc;
            }
            set
            {
                _updated_on_utc = value;
            }
        }

        #endregion
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DBSyncService.Entity
{
    public class OrderItem
    {
        #region Private variables

        private int _order_id;
        private string _item_code;
        private int _quantity;
        private decimal _unit_price;
        private decimal _total_price;
        private decimal _discount_price;
        private decimal _final_price;
        private DateTime _created_on_utc;
        private DateTime _updated_on_utc;

        #endregion

        #region Public variables

        public int OrderId
        {
            get
            {
                return _order_id;
            }
            set
            {
                _order_id = value;
            }
        }

        public string ItemCode
        {
            get
            {
                return _item_code;
            }
            set
            {
                _item_code = value;
            }
        }

        public int Quantity
        {
            get
            {
                return _quantity;
            }
            set
            {
                _quantity = value;
            }
        }

        public decimal UnitPrice
        {
            get
            {
                return _unit_price;
            }
            set
            {
                _unit_price = value;
            }
        }

        public decimal TotalPrice
        {
            get
            {
                return _total_price;
            }
            set
            {
                _total_price = value;
            }
        }

        public decimal DiscountPrice
        {
            get
            {
                return _discount_price;
            }
            set
            {
                _discount_price = value;
            }
        }

        public decimal FinalPrice
        {
            get
            {
                return _final_price;
            }
            set
            {
                _final_price = value;
            }
        }

        public DateTime CreatedOnUTC
        {
            get
            {
                return _created_on_utc;
            }
            set
            {
                _created_on_utc = value;
            }
        }

        public DateTime UpdatedOnUTC
        {
            get
            {
                return _updated_on_utc;
            }
            set
            {
                _updated_on_utc = value;
            }
        }

        #endregion
    }
}

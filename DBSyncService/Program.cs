﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading.Tasks;

namespace DBSyncService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            ServiceBase[] ServicesToRun;
            ServicesToRun = new ServiceBase[]
            {
                new DBSyncService()
            };
            ServiceBase.Run(ServicesToRun);

            //DBSyncService oDBSyncService = new DBSyncService();  // For locally debug. Comment while release.
        }
    }
}
